<?php
/**
 * Created by PhpStorm.
 * User: Minture Fok
 * Date: 2016/9/7
 * Time: 14:33
 */
class Video_model extends CI_Model{
    public $id;
    public $title;
    public $url;
    public $desc;
    public $create_date;
    public $pic_url;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('video', 10);
        return $query->result();
    }
}