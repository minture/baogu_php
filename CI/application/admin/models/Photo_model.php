<?php
/**
 * Created by PhpStorm.
 * User: Minture Fok
 * Date: 2016/9/7
 * Time: 14:33
 */
class Photo_model extends CI_Model{
    public $id;
    public $name;
    public $url;
    public $type;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('photo', 10);
        return $query->result();
    }
}