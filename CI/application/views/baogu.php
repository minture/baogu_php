<base href="<?php  echo base_url();?>"/>
<!DOCTYPE html>
<html xmlns:wb="http://open.weibo.com/wb">
<head>
<title>熊心瑶应援会</title>
<link href="public/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="public/js/jquery.min.js"></script>
<!--<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>-->
<!-- Custom Theme files -->
<!--theme-style-->
<link href="public/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="public/css/time.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="public/titlelogo.ico" type="image/x-icon">
<link rel="icon" href="public/titlelogo.ico" type="image/x-icon">
<!--//theme-style-->
<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="熊心瑶" />
	
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<!--link href='http://fonts.useso.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>-->
<!--<link href='http://fonts.useso.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>-->
<!--//fonts-->
<script type="text/javascript" src="public/js/move-top.js"></script>
<script type="text/javascript" src="public/js/easing.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
					</script>

</head>
<body>
<!--header-->
	<div class="header" id="home" >
		<div class="container">	
			<div class="logo">
				<h1><a href="index.php"><img src="public/images/logo.png" alt=""></a><span id="title">苞谷园</span></h1>
			</div>
			<div class="header-bottom">
				<div class="top-nav">
					<span class="menu"> </span>
					<ul>
						<li><a href="#home" class="scroll">首页</a></li>
						<li><a href="#portfolio" class="scroll">相册</a></li>
						<li><a href="#services" class="scroll">微博</a></li>
						<li><a href="#video" class="scroll">视频</a></li>
						<li><a href="contact.html">关于</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
			 <!-- start search-->
			<!--<div class="search-box">
			   <div id="sb-search" class="sb-search">
				  <form>
					 <input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
					 <input class="sb-search-submit" type="submit" value="">
					 <span class="sb-icon-search"> </span>
				  </form>
			    </div>
			 </div>-->
			 <!----search-scripts---->
			 <script src="public/js/classie.js"></script>
			 <script src="public/js/uisearch.js"></script>
			  <!-- <script>
				 new UISearch( document.getElementById( 'sb-search' ) );
			   </script>-->
				<!----//search-scripts---->
			
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		</div>
	</div>
	<!---->
	
	<div class="container">		
		<div class="banner">
			<div class="banner-matter">
				<h2>熊心瑶</h2>
				<p>我来自重庆。大家可以叫我心瑶或者Haruka我实在是太喜欢偶像了，所以成为了偶像想成为跳舞很棒、有趣的偶像。虽然现在还是个跳舞不好又不有趣的人，也希望大家能关注我、和我一起成长</p>
			 </div>
			<div class="main">
				<img class="img-responsive" src="public/images/xxyQQ.png" alt="">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!---->
	<div class="container">	
	<div class="works" id="portfolio">
			<div class=" port-top">									
				<h3 class="port">相册</h3>
					<ul id="filters">
						<li class="active"><span class="filter " data-filter="app card icon set sit web">全部</span></li>
						<li><span class="filter" data-filter="video">日常</span></li>
						<li><span class="filter" data-filter="card"> 返图 </span></li>
						<li><span class="filter" data-filter="icon"> 七夕</span></li>
						<li><span class="filter" data-filter="set"> xx</span></li>
						<li><span class="filter" data-filter="add"> xx</span></li>
					</ul>
					<div class="clearfix"> </div>
				</div>
					<div id="portfoliolist">
						<div class="portfolio video mix_all" data-cat="video" style="display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper ">
								<a href="single.html" class="b-link-stripe b-animate-go">
								<img class="img-responsive" src="public/images/v1.jpg" alt=""  />
									<div class="b-wrapper">
										<h2 class="b-animate b-from-left ">
											<span>日常照片</span>
											<button>查看更多</button>
										</h2>
									</div>
							</a>
							</div>
						</div>
						<div class="portfolio icon mix_all" data-cat="icon" style="display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">
								<a href="single.html" class="b-link-stripe b-animate-go">
								<img class="img-responsive" src="public/images/v1.jpg" alt=""  />
								<div class="b-wrapper" class="b-link-stripe b-animate-go">
										<h2 class="b-animate b-from-left ">
											<span>日常照片</span>
											<button>查看更多</button>
										</h2>
									</div>
							</a>
							</div>
						</div>

						<div class="portfolio card mix_all" data-cat="card" style="display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">
								<a href="single.html" class="b-link-stripe b-animate-go">
								 <img class="img-responsive" src="public/images/v1.jpg"  alt="" />
								 <div class="b-wrapper">
										<h2 class="b-animate b-from-left ">
											<span>日常照片</span>
											<button>查看更多</button>
										</h2>
									</div>
							</a>

							</div>
						</div>


						<div class="portfolio video mix_all" data-cat="video" style="display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper ">
								<a href="single.html" class="b-link-stripe b-animate-go">
								 <img class="img-responsive" src="public/images/v1.jpg" alt=""  />
								 <div class="b-wrapper">
										<h2 class="b-animate b-from-left ">
											<span>日常照片</span>
											<button>查看更多</button>
										</h2>
									</div>
							</a>
					</div>
						</div>
						<div class="portfolio add mix_all" data-cat="add" style="display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper ">
								<a href="single.html" class="b-link-stripe b-animate-go">
								 <img class="img-responsive" src="public/images/v1.jpg" alt=""  />
									<div class="b-wrapper">
										<h2 class="b-animate b-from-left ">
											<span>日常照片</span>
											<button>查看更多</button>
										</h2>
									</div>
							</a>

							</div>
						</div>
						<div class="portfolio set mix_all" data-cat="icon" style="display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">
								<a href="single.html" class="b-link-stripe b-animate-go">
								 <img class="img-responsive" src="public/images/v1.jpg" alt=""  />
								 <div class="b-wrapper">
										<h2 class="b-animate b-from-left ">
											<span>日常照片</span>
											<button>查看更多</button>
										</h2>
									</div>

							</a>

							</div>
						</div>
						<div class="clearfix"> </div>
					</div>	
			<!-- Script for gallery Here -->
				<script type="text/javascript" src="public/js/jquery.mixitup.min.js"></script>
					<script type="text/javascript">
					$(function () {
						
						var filterList = {
						
							init: function () {
							
								// MixItUp plugin
								// http://mixitup.io
								$('#portfoliolist').mixitup({
									targetSelector: '.portfolio',
									filterSelector: '.filter',
									effects: ['fade'],
									easing: 'snap',
									// call the hover effect
									onMixEnd: filterList.hoverEffect()
								});				
							
							},
							
							hoverEffect: function () {
							
								// Simple parallax effect
								$('#portfoliolist .portfolio').hover(
									function () {
										$(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
										$(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
									},
									function () {
										$(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
										$(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
									}		
								);				
							
							}
				
						};
						
						// Run the show!
						filterList.init();
					});
					var Tabbtn=$("#filters li");
					Tabbtn.each(function(k,elem){
						Tabbtn.eq(k).click(function(){
							Tabbtn.eq(k).addClass("current").sliblings.removeClass("current");
						})
					})
					</script>
			<!-- Gallery Script Ends -->
			</div>
		</div>

			<!---->
		<div class="container">	
			<div class="service" id="services">
				<div class=" service-top">									
					<h3 class="ser">我们的心瑶</h3>
					<p class="sed"> </p>
					<div class="clearfix"> </div>
				</div>
				<div class=" services-grid">

					<div class="col-md-6 service-grid">
						<iframe width="100%" height="550" class="share_self"  frameborder="0" scrolling="no" src="http://widget.weibo.com/weiboshow/index.php?language=&width=0&height=550&fansRow=2&ptype=1&speed=0&skin=1&isTitle=1&noborder=1&isWeibo=1&isFans=1&uid=5863793218&verifier=2e9cca03&dpc=1"></iframe>
					</div>
					<div class="col-md-6 service-grid" id="history">
						<section id="cd-timeline" class="cd-container">
							<div class="cd-timeline-block">
								<div class="cd-timeline-img cd-picture">
									<img src="http://7xnlea.com1.z0.glb.clouddn.com/cd-icon-picture.svg" alt="Picture">
								</div>

								<div class="cd-timeline-content">
									<h2>HTML5+CSS3实现的响应式垂直时间轴</h2>
									<p>网页时间轴一般用于展示以时间为主线的事件，如企业网站常见的公司发展历程等。本文将给大家介绍一款基于HTML5和CSS3的漂亮的垂直时间轴，它可以响应页面布局，适用于HTML5开发的PC和移动手机WEB应用。</p>
									<a href="http://www.helloweba.com/view-blog-285.html" class="cd-read-more" target="_blank">阅读全文</a>
									<span class="cd-date">2015-01-06</span>
								</div>
							</div>
							<div class="cd-timeline-block">
								<div class="cd-timeline-img cd-movie">
									<img src="http://7xnlea.com1.z0.glb.clouddn.com/cd-icon-movie.svg" alt="Movie">
								</div>

								<div class="cd-timeline-content">
									<h2>jQuery+PHP动态数字展示效果</h2>
									<p>我们在一些应用中需要动态展示数据，比如当前在线人数，当前交易总额，当前汇率等等，前端页面需要实时刷新获取最新数据。本文将结合实例给大家介绍使用jQuery和PHP来实现动态数字展示效果。</p>
									<a href="http://www.helloweba.com/view-blog-284.html" class="cd-read-more" target="_blank">阅读全文</a>
									<span class="cd-date">2014-12-25</span>
								</div>
							</div>
							<div class="cd-timeline-block">
								<div class="cd-timeline-img cd-picture">
									<img src="http://7xnlea.com1.z0.glb.clouddn.com/cd-icon-picture.svg" alt="Picture">
								</div>

								<div class="cd-timeline-content">
									<h2>PHP操作Session和Cookie</h2>
									<p>我们跟踪用户信息时需要用到Session和Cookie，比如用户登录验证、记录用户浏览历史，存储购物车数据，限制用户会话有效时间等。今天我们来了解下PHP是如何操作Session和Cookie的。</p>
									<a href="http://www.helloweba.com/view-blog-283.html" class="cd-read-more" target="_blank">阅读全文</a>
									<span class="cd-date">2014-12-20</span>
								</div>
							</div>
							<div class="cd-timeline-block">
								<div class="cd-timeline-img cd-movie">
									<img src="http://7xnlea.com1.z0.glb.clouddn.com/cd-icon-movie.svg" alt="Movie">
								</div>

								<div class="cd-timeline-content">
									<h2>jQuery数字加减插件</h2>
									<p>我们在网上购物提交订单时，在网页上一般会有一个选择数量的控件，要求买家选择购买商品的件数，开发者会把该控件做成可以通过点击实现加减等微调操作，当然也可以直接输入数字件数。本文将介绍常见的几种使用spinner数字微调器来实现数字加减的功能的方法。</p>
									<a href="http://www.helloweba.com/view-blog-282.html" class="cd-read-more" target="_blank">阅读全文</a>
									<span class="cd-date">2014-12-14</span>
								</div>
							</div>
							<div class="cd-timeline-block">
								<div class="cd-timeline-img cd-movie">
									<img src="http://7xnlea.com1.z0.glb.clouddn.com/cd-icon-location.svg" alt="Location">
								</div>

								<div class="cd-timeline-content">
									<h2>收集整理的非常有用的PHP函数</h2>
									<p>项目中经常会需要一些让人头疼的函数，作为开发者应该整理一个自己的函数库，在需要之时复制过来即可。本文作者收集整理数十个PHP项目中常用的函数，保证能正常运行，你只要复制粘贴到你项目中即可。</p>
									<a href="http://www.helloweba.com/view-blog-281.html" class="cd-read-more" target="_blank">阅读全文</a>
									<span class="cd-date">2014-12-05</span>
								</div>
							</div>
						</section>

					</div>
				<div class="clearfix"> </div>
				</div>
			</div>
		</div>
<!--视频-->
		<div class="container">
			<div class="service" id="video">
				<div class=" service-top">
					<h3 class="ser">闪耀瞬间</h3>
					<p class="sed">
						<ul id="myTab" class="tab-title">
							<li>公演</li>
							<li class="active">长隆</li>
						</ul>
					</p>
					<div class="clearfix"></div>
				</div>
				<div class=" services-grid">
					<div id="myTabContent" class="tab_container">
						<div class="tab-pane fade in active video-list tab-content" style="display:block;" id="gongyan" style="display: block; ">
							<ul>

								<li><a target="_blank" href="/index_video_show2.action?id=118&amp;title=悠闲的一天！" class="img"><img src="public/images/v1.jpg" width="210" height="150"> <span class="mask-video-icon"><i class="fa fa-play"></i></span> </a></li>
								<li><a target="_blank" href="/index_video_show2.action?id=118&amp;title=悠闲的一天！" class="img"><img src="public/images/v1.jpg" width="210" height="150"> <span class="mask-video-icon"><i class="fa fa-play"></i></span> </a></li>
								<?php foreach ($videos as $item):?>
									<li><a target="_blank" href="/index_video_show2.action?id=118&amp;title=悠闲的一天！" class="img"><img src="public/images/v1.jpg" width="210" height="150"> <span class="mask-video-icon"><i class="fa fa-play"></i></span> </a></li>
								<?php endforeach;?>
							</ul>
						</div>
						<div class="tab-pane fade tab-content" id="waiwu" style="display: block; ">
							<p>还没弄啊麻烦等等</p>
						</div>

					</div>

					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
			<!---->
		<div class="container">
			<div class="meet">
				<div class=" team-top">									
					<h3 class="team">加入我们</h3>
					<div class="clearfix"> </div>
				</div>
				<div class=" team-grid">
					<div class="col-md-4  grid-team">
						<a href="http://jq.qq.com/?_wv=1027&k=2DtH8pq"><img class="img-responsive" src="public/images/qrcode.jpg" alt=" "></a>
						<h4>熊心瑶应援QQ群</h4>
						<h5>543392670</h5>
					</div>
					<div class="col-md-4  grid-team">
						<a href="http://weibo.com/u/5863793218"><img class="img-responsive" src="public/images/me.png" alt=" "></a>
						<h4>新浪微博</h4>
						<h5>熊心瑶应援会</h5>
					</div>
					<div class="col-md-4  grid-team">
						<a href="single.html"><img class="img-responsive" src="public/images/maojin.png" alt=" "></a>
						<h4>应援毛巾</h4>
						<h5>淘宝店</h5>
					</div>		
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
			<!---->
			<div class="footer" >
			    <div class="container">
				<div class="footer-top" >
				<div class="col-md-12 twitter">
						
						<h4>友情链接</h4>
							<div class="twitter-in">
								<a href="#" >曾艳芬应援网 </a>
								|
								<a href="#" >龚诗琪应援网</a>
							</div>
						</div>
						<div class="clearfix"></div>
						</div>
			<div class="footer-bottom">
				<p class="footer-class">熊心瑶应援会</p>
				<div class="clearfix"></div>
			</div>						
		</div>
		 <script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	</div>
</body>
<script>

	$(document).ready(function(){
		var $li = $('#myTab li');
		var $ul = $('#myTabContent div');

		$li.click(function(){
			var $this = $(this);
			var $t = $this.index();
			$li.removeClass();
			$this.addClass('current');
			$ul.css('display','none');
			$ul.eq($t).css('display','block');
		})

	});

</script>
</html>