<!DOCTYPE html>
<html xmlns:wb="http://open.weibo.com/wb">
<head>
<title>熊心瑶应援会</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="titlelogo.ico" type="image/x-icon">
<link rel="icon" href="titlelogo.ico" type="image/x-icon">
<!--//theme-style-->
<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="熊心瑶" />
	
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<!--link href='http://fonts.useso.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>-->
<!--<link href='http://fonts.useso.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>-->
<!--//fonts-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
					</script>

</head>
<body>
<!--header-->
	<div class="header" id="home" >
		<div class="container">	
			<div class="logo">
				<h1><a href="index.php"><img src="images/logo.png" alt=""></a><span id="title">苞谷园</span></h1>
			</div>
			<div class="header-bottom">
				<div class="top-nav">
					<span class="menu"> </span>
					<ul>
						<li><a href="#home" class="scroll">首页</a></li>
						<li><a href="#portfolio" class="scroll">相册</a></li>
						<li><a href="#services" class="scroll">微博</a></li>
						<li><a href="#video" class="scroll">视频</a></li>
						<li><a href="contact.html">关于</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
				</div>
			 <!-- start search-->
			<!--<div class="search-box">
			   <div id="sb-search" class="sb-search">
				  <form>
					 <input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
					 <input class="sb-search-submit" type="submit" value="">
					 <span class="sb-icon-search"> </span>
				  </form>
			    </div>
			 </div>-->
			 <!----search-scripts---->
			 <script src="js/classie.js"></script>
			 <script src="js/uisearch.js"></script>
			   <script>
				 new UISearch( document.getElementById( 'sb-search' ) );
			   </script>
				<!----//search-scripts---->
			
			<div class="clearfix"> </div>
		</div>
		<div class="clearfix"> </div>
		</div>
	</div>
	<!---->
	
	<div class="container">		
		<div class="banner">
			<div class="banner-matter">
				<h2>熊心瑶</h2>
				<p>我来自重庆。大家可以叫我心瑶或者Haruka我实在是太喜欢偶像了，所以成为了偶像想成为跳舞很棒、有趣的偶像。虽然现在还是个跳舞不好又不有趣的人，也希望大家能关注我、和我一起成长</p>
			 </div>
			<div class="main">
				<img class="img-responsive" src="images/xxyQQ.png" alt="">
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!---->
	<div class="container">	
	<div class="works" id="portfolio">
			<div class=" port-top">									
				<h3 class="port">相册</h3>
					<ul id="filters">
						<li class="active"><span class="filter " data-filter="app card icon set sit web">全部</span></li>
						<li><span class="filter" data-filter="video">日常</span></li>
						<li><span class="filter" data-filter="card"> 返图 </span></li>
						<li><span class="filter" data-filter="icon"> 七夕</span></li>
						<li><span class="filter" data-filter="set"> xx</span></li>
						<li><span class="filter" data-filter="add"> xx</span></li>
					</ul>
					<div class="clearfix"> </div>
				</div>
					<div id="portfoliolist">
					<div class="portfolio video mix_all" data-cat="video" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper ">		
							<a href="single.html" class="b-link-stripe b-animate-go">
						    <img class="img-responsive" src="images/v1.jpg" alt=""  /> 
								<div class="b-wrapper">
									<h2 class="b-animate b-from-left ">	
										<span>日常照片</span>									
										<button>查看更多</button>
									</h2>
								</div>
						</a>																			
						</div>
					</div>				
					<div class="portfolio icon mix_all" data-cat="icon" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">		
							<a href="single.html" class="b-link-stripe b-animate-go">
						    <img class="img-responsive" src="images/v1.jpg" alt=""  />
						   	<div class="b-wrapper" class="b-link-stripe b-animate-go">
									<h2 class="b-animate b-from-left ">	
										<span>日常照片</span>									
										<button>查看更多</button>
									</h2>
								</div>
						</a>	
							
		                </div>
					</div>
						
					<div class="portfolio card mix_all" data-cat="card" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">		
							<a href="single.html" class="b-link-stripe b-animate-go">
						     <img class="img-responsive" src="images/v1.jpg"  alt="" />
						     <div class="b-wrapper">
									<h2 class="b-animate b-from-left ">	
										<span>日常照片</span>									
										<button>查看更多</button>
									</h2>
								</div>
						</a>	
							
		                </div>
					</div>

							
					<div class="portfolio video mix_all" data-cat="video" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper ">		
							<a href="single.html" class="b-link-stripe b-animate-go">
						     <img class="img-responsive" src="images/v1.jpg" alt=""  />
						     <div class="b-wrapper">
									<h2 class="b-animate b-from-left ">	
										<span>日常照片</span>									
										<button>查看更多</button>
									</h2>
								</div>
						</a>							
	            </div>
					</div>	
					<div class="portfolio add mix_all" data-cat="add" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper ">		
							<a href="single.html" class="b-link-stripe b-animate-go">
						     <img class="img-responsive" src="images/v1.jpg" alt=""  />
						    	<div class="b-wrapper">
									<h2 class="b-animate b-from-left ">	
										<span>日常照片</span>									
										<button>查看更多</button>
									</h2>
								</div>	
						</a>	
								
		                </div>
					</div>			
					<div class="portfolio set mix_all" data-cat="icon" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper">		
							<a href="single.html" class="b-link-stripe b-animate-go">
						     <img class="img-responsive" src="images/v1.jpg" alt=""  />
						     <div class="b-wrapper">
									<h2 class="b-animate b-from-left ">	
										<span>日常照片</span>									
										<button>查看更多</button>
									</h2>
								</div>
						    	
						</a>	
							
		                </div>
					</div>
					<div class="clearfix"> </div>
					</div>	
			<!-- Script for gallery Here -->
				<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
					<script type="text/javascript">
					$(function () {
						
						var filterList = {
						
							init: function () {
							
								// MixItUp plugin
								// http://mixitup.io
								$('#portfoliolist').mixitup({
									targetSelector: '.portfolio',
									filterSelector: '.filter',
									effects: ['fade'],
									easing: 'snap',
									// call the hover effect
									onMixEnd: filterList.hoverEffect()
								});				
							
							},
							
							hoverEffect: function () {
							
								// Simple parallax effect
								$('#portfoliolist .portfolio').hover(
									function () {
										$(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
										$(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
									},
									function () {
										$(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
										$(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
									}		
								);				
							
							}
				
						};
						
						// Run the show!
						filterList.init();
					});	
					</script>
			<!-- Gallery Script Ends -->
			</div>
		</div>

			<!---->
		<div class="container">	
			<div class="service" id="services">
				<div class=" service-top">									
					<h3 class="ser">我们的心瑶</h3>
					<p class="sed">左边应该是应援博 </p>
					<div class="clearfix"> </div>
				</div>
				<div class=" services-grid">
					<div class="col-md-6 service-grid">
						<iframe width="100%" height="550" class="share_self"  frameborder="0" scrolling="no" src="http://widget.weibo.com/weiboshow/index.php?language=&width=0&height=550&fansRow=2&ptype=1&speed=0&skin=1&isTitle=1&noborder=1&isWeibo=1&isFans=1&uid=5863793218&verifier=2e9cca03&dpc=1"></iframe>
					</div>
					<div class="col-md-6 service-grid">
						<a href="single.html"><img class="img-responsive" src="images/s3.png" alt=" "></a>
						<h5>这里不知道弄什么好</h5>
						<p>这里不知道弄什么好</p>
					</div>		
				<div class="clearfix"> </div>
				</div>
			</div>
		</div>
<!--视频-->
		<div class="container">
			<div class="service" id="video">
				<div class=" service-top">
					<h3 class="ser">闪耀瞬间</h3>
					<p class="sed"></p>
					<div class="clearfix"> </div>
				</div>
				<div class=" services-grid">
					<embed height="415" width="544" quality="high" allowfullscreen="true" type="application/x-shockwave-flash" src="http://static.hdslb.com/miniloader.swf" flashvars="aid=5281347&page=3" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"></embed>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
			<!---->
		<div class="container">
			<div class="meet">
				<div class=" team-top">									
					<h3 class="team">加入我们</h3>
					<div class="clearfix"> </div>
				</div>
				<div class=" team-grid">
					<div class="col-md-4  grid-team">
						<a href="http://jq.qq.com/?_wv=1027&k=2DtH8pq"><img class="img-responsive" src="images/qrcode.jpg" alt=" "></a>
						<h4>熊心瑶应援QQ群</h4>
						<h5>543392670</h5>
					</div>
					<div class="col-md-4  grid-team">
						<a href="single.html"><img class="img-responsive" src="images/me.png" alt=" "></a>
						<h4>新浪微博</h4>
						<h5>熊心瑶应援会</h5>
					</div>
					<div class="col-md-4  grid-team">
						<a href="single.html"><img class="img-responsive" src="images/maojin.png" alt=" "></a>
						<h4>应援毛巾</h4>
						<h5>淘宝店</h5>
					</div>		
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
			<!---->
			<div class="footer" >
			    <div class="container">
				<div class="footer-top" >
				<div class="col-md-12 twitter">
						
						<h4>友情链接</h4>
							<div class="twitter-in">
								<a href="#" >曾艳芬应援网 </a>
								|
								<a href="#" >龚诗琪应援网</a>
							</div>
						</div>
						<div class="clearfix"></div>
						</div>
			<div class="footer-bottom">
				<p class="footer-class">熊心瑶应援会</p>
				<div class="clearfix"></div>
			</div>						
		</div>
		 <script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	</div>
</body>
</html>